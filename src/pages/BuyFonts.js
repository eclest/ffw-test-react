import React, { useContext } from 'react'
import { ContextApp } from '../store/reducer.js'

export const BuyFonts = (props) => {

    const { state } = useContext(ContextApp)

    return (
        <div className='buy-fonts-container'>
            {state.text}
        </div>
    )
}