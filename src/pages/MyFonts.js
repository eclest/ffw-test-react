import React, { useContext } from 'react'
import { ContextApp } from '../store/reducer.js'
import { FontItem } from '../components/FontItem/FontItem'

export const MyFonts = (props) => {

    const { state } = useContext(ContextApp)

    return (
        <div className='my-fonts-container'>
            {state.fonts && state.fonts.map((el, index) => {
                return (
                    <FontItem key={index} item={el} />
                )
            })}
        </div>
    )
}