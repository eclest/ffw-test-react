import services from '../services'

export const getTabs = async (dispatch) => {
    let response = await services.api.fonts.getContent('tabs')
    if (response.isSuccess) {
        dispatch({ type: 'set_tabs_data', payload: response.data })
    } else {
        console.log(response)
    }
}

export const getMyFonts = async (url, dispatch) => {
    let response = await services.api.fonts.getMyFonts(url)
    if (response.isSuccess) {
        dispatch({ type: 'set_fonts', payload: response.data.content })
    } else {
        console.log(response)
    }
}

export const getBuyFonts = async (url, dispatch) => {
    let response = await services.api.fonts.getBuyFonts(url)
    if (response.isSuccess) {
        dispatch({ type: 'set_text', payload: response.data.content })
    } else {
        console.log(response)
    }
}

export const setActiveFont = (id, dispatch) => dispatch({ type: 'set_active_id', payload: id })