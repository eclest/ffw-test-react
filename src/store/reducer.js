import React from 'react';

export const ContextApp = React.createContext();

export const initialState = {
    tabs: [],
    fonts: [],
    text: '',
    activeFontId: undefined,
}

export const appReducer = (state, action) => {
    switch (action.type) {

        case 'set_tabs_data':
            return {
                ...state,
                tabs: action.payload
            };

        case 'set_fonts':
            return {
                ...state,
                fonts: action.payload
            };

        case 'set_text':
            return {
                ...state,
                text: action.payload
            }

        case 'set_active_id':
            return {
                ...state,
                activeFontId: action.payload
            }

        default: return state;
    }
}
