import React, { useReducer } from 'react';
import { ContextApp, appReducer, initialState } from './store/reducer.js'
import { BrowserRouter as Router } from "react-router-dom";
import { FontTabs } from './components/FontTabs/FontTabs'
import './App.css'

export const App = () => {

  const [state, dispatch] = useReducer(appReducer, initialState);

  return (
    <ContextApp.Provider value={{ dispatch, state }}>
      <Router>
        <FontTabs />
      </Router>
    </ContextApp.Provider>
  );
}

export default App;
