import axios from 'axios';

export class ApiResponse {
    status: number;
    data: any;
    isSuccess: boolean;
    isError: boolean;

    constructor(status: number, data: any, headers?: any) {
        this.status = status;
        this.data = data;
        this.headers = headers;

        this.isSuccess = status >= 200 && status <= 299;
        this.isError = !this.isSuccess;
    }

    get(x: string) {
        return this.data[x];
    }
}

export const request = (options: any): Promise<ApiResponse> => {

    return axios
        .request({
            baseURL: process.env.REACT_APP_API_URL,
            responseType: 'json',
            ...options,
        })
        .then(function (response) {
            const { status, data, headers } = response;

            return new ApiResponse(status, data, headers);
        })
        .catch(function (error) {
            if (error.response) {
                const { status, data } = error.response;

                return new ApiResponse(status, data);
            }
            return new ApiResponse(400, {});
        });
};