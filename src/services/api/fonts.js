import { request } from './base';

export const getContent = (url) => {
    return request({
        method: 'get',
        url,
    })
}

export const getMyFonts = (url) => {
    return request({
        method: 'get',
        url
    })
}

export const getBuyFonts = (url) => {
    return request({
        method: 'get',
        url
    })
}