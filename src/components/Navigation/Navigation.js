import React, { useState, useEffect, useContext } from 'react'
import { NavLink } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { getTabs, getMyFonts, getBuyFonts } from '../../store/actions.js'
import { ContextApp } from '../../store/reducer.js'
import './Navigation.css'

export const Navigation = (props) => {

    let location = useLocation();

    const { state, dispatch } = useContext(ContextApp)

    const [title, setTitle] = useState('')

    useEffect(() => {
        location.pathname === '/' ? setTitle('Please select one font') : setTitle('')
        if (state.tabs.length && location.pathname === '/') getMyFonts(state.tabs.find(el => el.label === 'My Fonts').content_endpoint, dispatch)
        if (state.tabs.length && location.pathname === '/buy-fonts') getBuyFonts(state.tabs.find(el => el.label === 'Buy Fonts').content_endpoint, dispatch)
    }, [location, state.tabs])

    useEffect(() => {
        getTabs(dispatch)
    }, [])

    return (
        <div className='navigation-container'>
            {title}
            <div className='links-container'>
                <NavLink exact activeClassName='active-link' to='/'>My Fonts</NavLink>
                <NavLink activeClassName='active-link' to='/buy-fonts'>Buy Fonts</NavLink>
            </div>
        </div>
    )
}