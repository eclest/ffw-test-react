import React, { useContext } from 'react'
import { ContextApp } from '../../store/reducer.js'
import { setActiveFont } from '../../store/actions.js'
import './FontItem.css'

export const FontItem = ({ item }) => {

    const { state, dispatch } = useContext(ContextApp)

    const handleSelection = (e) => {
        e.preventDefault();
        setActiveFont(Number(e.currentTarget.id), dispatch)
    }

    return (
        <div className={`font-item-container ${state.activeFontId === item.id ? 'active-font' : null}`} id={item.id} onClick={handleSelection}>
            <div className='font-item-white-container'>
                <div className='font-color-container' aria-labelledby={item['color-blind-label']} style={{ background: `${item.color}` }}>
                    <div>{item.abbr}</div>
                </div>
            </div>
            <ul>
                <li>{item.label}</li>
            </ul>
        </div>
    )
}