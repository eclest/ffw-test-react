import React from 'react';
import { Switch, Route } from "react-router-dom";
import { BuyFonts } from '../../pages/BuyFonts'
import { MyFonts } from '../../pages/MyFonts'
import { Navigation } from '../Navigation/Navigation'
import './FontTabs.css'

export const FontTabs = (props) => {

    return (
        <div className='app-container'>
            <Navigation />
            <div className='pages-container'>
                <Switch>
                    <Route exact path='/'>
                        <MyFonts />
                    </Route>
                    <Route path='/buy-fonts'>
                        <BuyFonts />
                    </Route>
                </Switch>
            </div>
        </div>
    );
}

export default FontTabs;
